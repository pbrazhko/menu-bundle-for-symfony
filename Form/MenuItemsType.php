<?php

namespace CMS\MenuBundle\Form;

use CMS\LocalizationBundle\Form\Types\LocaleEntityType;
use CMS\LocalizationBundle\Form\Types\LocaleTextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MenuItemsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', LocaleTextType::class)
            ->add('alias')
            ->add('parent', LocaleEntityType::class, array(
                'class' => 'MenuBundle:MenuItems',
                'choice_label' => 'title',
                'empty_data' => null,
                'placeholder' => '',
                'required' => false
            ))
            ->add('url', TextType::class, array(
                'required' => false
            ))
            ->add('menu_type', LocaleEntityType::class, array(
                'class' => 'MenuBundle:MenuTypes',
                'choice_label' => 'title',
                'empty_data' => null,
                'placeholder' => ''
            ))
            ->add('roles', EntityType::class, array(
                'class' => 'UsersBundle:Roles',
                'choice_label' => 'name',
                'multiple' => true,
                'empty_data' => null,
                'placeholder' => ''
            ))
            ->add('sort')
            ->add('css_classes')
            ->add('is_published')
            ->add('is_deleted');
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CMS\MenuBundle\Entity\MenuItems',
            'translation_domain' => 'systems'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'cms_menubundle_menuitems';
    }
}
