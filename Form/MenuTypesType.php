<?php

namespace CMS\MenuBundle\Form;

use CMS\LocalizationBundle\Form\Types\LocaleTextareaType;
use CMS\LocalizationBundle\Form\Types\LocaleTextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MenuTypesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', LocaleTextType::class)
            ->add('alias')
            ->add('description', LocaleTextareaType::class, array(
                'required' => false
            ))
            ->add('sort')
            ->add('is_published')
            ->add('is_deleted');
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CMS\MenuBundle\Entity\MenuTypes',
            'translation_domain' => 'systems'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'cms_menubundle_menutypes';
    }
}
