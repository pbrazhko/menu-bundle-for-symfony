<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 29.01.15
 * Time: 12:29
 */
namespace CMS\MenuBundle\Twig;

use CMS\MenuBundle\Entity\MenuItems;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class MenuTwigExtension extends \Twig_Extension
{
    private $container;
    
    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('cms_show_menu', [$this, 'showMenu'], ['is_safe' => ['html'], 'needs_environment' => true])
        );
    }

    public function getTests()
    {
        return [
            new \Twig_SimpleTest('is_menu_item_active', [$this, 'isMenuItemActive'])
        ];
    }

    public function isMenuItemActive(MenuItems $item)
    {
        $requestStack = $this->container->get('request_stack');
        $request = $requestStack->getCurrentRequest();

        $requestUri = $request->getRequestUri();

        if (strpos($requestUri, '/' . $request->getLocale() . $item->getUrl()) === 0) {
            return true;
        }

        return false;
    }

    public function showMenu(\Twig_Environment $environment, $menuAlias, $parentAlias = null, $template = 'MenuBundle:Twig:showMenu.html.twig')
    {
        $serviceMenuItems = $this->container->get('cms.menu.items.service');

        $menuItemsRepository = $serviceMenuItems->getRepository();
        $builder = $menuItemsRepository->createQueryBuilder('mi')
            ->select(array('mi', 'mir'))
            ->innerJoin('mi.roles', 'mir')
            ->innerJoin('mi.menu_type', 'mt', Join::WITH, 'mt.alias = :menu_type_alias')
            ->andWhere('mi.is_published = 1')
            ->andWhere('mi.is_deleted = 0')
            ->orderBy('mi.sort', 'asc')
            ->setParameter('menu_type_alias', $menuAlias);

        if (null !== $parentAlias) {
            $builder->innerJoin('mi.parent', 'mip', Join::WITH, 'mip.alias = :parent')
                ->setParameter('parent', $parentAlias);
        }

        $query = $builder
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true)
            ->setQueryCacheLifetime(86400)
            ->setResultCacheLifetime(86400);

        $result = $query->getResult();

        return $environment->render($template, array(
            'items' => $result ?: array()
        ));
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'twig_menu_extension';
    }
}