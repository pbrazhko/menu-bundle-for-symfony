<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 15.01.15
 * Time: 15:34
 */

namespace CMS\MenuBundle\Services;


use CMS\CoreBundle\AbstractCoreService;
use CMS\MenuBundle\Form\MenuTypesType;
use Symfony\Component\Form\FormFactory;

class MenuTypesService extends AbstractCoreService
{

    /**
     * @return array
     */
    public function getDefaultsCriteria()
    {
        return array(
            'is_deleted' => false
        );
    }

    public function configureForm(FormFactory $form, $data = null)
    {
        return $form->createBuilder(
            MenuTypesType::class,
            $data,
            array(
                'data_class' => $this->getRepositoryClass()
            )
        );
    }

    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName()
    {
        return 'MenuBundle:MenuTypes';
    }
}