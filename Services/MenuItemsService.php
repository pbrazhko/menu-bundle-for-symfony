<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 15.01.15
 * Time: 15:48
 */

namespace CMS\MenuBundle\Services;


use CMS\CoreBundle\AbstractCoreService;
use CMS\MenuBundle\Form\MenuItemsType;
use Symfony\Component\Form\FormFactory;

class MenuItemsService extends AbstractCoreService
{

    /**
     * @return array
     */
    public function getDefaultsCriteria()
    {
        return array(
            'is_deleted' => false
        );
    }

    /**
     * Return form for entity
     *
     * @param FormBuilder|FormFactory $form
     * @param null $data
     * @return mixed
     */
    public function configureForm(FormFactory $form, $data = null)
    {
        return $form->createBuilder(
            MenuItemsType::class,
            $data,
            array(
                'data_class' => $this->getRepositoryClass()
            )
        );
    }

    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName()
    {
        return 'MenuBundle:MenuItems';
    }
}