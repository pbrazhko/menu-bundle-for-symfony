<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 12.01.15
 * Time: 16:49
 */

namespace CMS\MenuBundle;


use Symfony\Component\HttpKernel\Bundle\Bundle;

class MenuBundle extends Bundle{
    public function isEnabled(){
        return true;
    }

    public function getDescription(){
        return array(
            array(
                'title' => 'Menu items',
                'defaultRoute' => 'cms_menu_list'
            ),
            array(
                'title' => 'Menu types',
                'defaultRoute' => 'cms_menu_types_list'
            )
        );
    }
}