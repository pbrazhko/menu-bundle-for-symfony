<?php

namespace CMS\MenuBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * MenuItems
 */
class MenuItems
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $menu_type;

    /**
     * @var integer
     */
    private $parent;

    /**
     * @var ArrayCollection
     */
    private $childs;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $alias;

    /**
     * @var string
     */
    private $url;

    /**
     * @var ArrayCollection
     */
    private $roles;

    /**
     * @var int
     */
    private $sort = 0;

    /**
     * @var string
     */
    private $css_classes;

    /**
     * @var boolean
     */
    private $is_published;

    /**
     * @var boolean
     */
    private $is_deleted;

    /**
     * @var \DateTime
     */
    private $date_create;

    /**
     * @var \DateTime
     */
    private $date_update;

    /**
     * @var integer
     */
    private $create_by;

    /**
     * @var integer
     */
    private $update_by;

    public function __construct()
    {
        $this->childs = new ArrayCollection();
        $this->roles = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getMenuType()
    {
        return $this->menu_type;
    }

    /**
     * @param mixed $menu_type
     * @return $this
     */
    public function setMenuType($menu_type)
    {
        $this->menu_type = $menu_type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     * @return $this
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getChilds()
    {
        return $this->childs;
    }

    /**
     * @param ArrayCollection $childs
     * @return $this
     */
    public function setChilds($childs)
    {
        $this->childs = $childs;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return MenuItems
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param ArrayCollection $roles
     * @return MenuItems
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @return int
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     * @return MenuItems
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * @return string
     */
    public function getCssClasses()
    {
        return $this->css_classes;
    }

    /**
     * @param string $css_classes
     *
     * @return MenuItems
     */
    public function setCssClasses($css_classes)
    {
        $this->css_classes = $css_classes;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     * @return MenuItems
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsPublished()
    {
        return $this->is_published;
    }

    /**
     * @param boolean $is_published
     * @return $this
     */
    public function setIsPublished($is_published)
    {
        $this->is_published = $is_published;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * @param boolean $is_deleted
     * @return $this
     */
    public function setIsDeleted($is_deleted)
    {
        $this->is_deleted = $is_deleted;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->date_create;
    }

    /**
     * @param \DateTime $date_create
     * @return $this
     */
    public function setDateCreate($date_create)
    {
        $this->date_create = new \DateTime();

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->date_update;
    }

    /**
     * @param \DateTime $date_update
     * @return $this
     */
    public function setDateUpdate($date_update)
    {
        $this->date_update = new \DateTime();

        return $this;
    }

    /**
     * @return int
     */
    public function getCreateBy()
    {
        return $this->create_by;
    }

    /**
     * @param int $create_by
     * @return $this
     */
    public function setCreateBy($create_by)
    {
        $this->create_by = $create_by;

        return $this;
    }

    /**
     * @return int
     */
    public function getUpdateBy()
    {
        return $this->update_by;
    }

    /**
     * @param int $update_by
     * @return $this
     */
    public function setUpdateBy($update_by)
    {
        $this->update_by = $update_by;
        return $this;
    }


}
