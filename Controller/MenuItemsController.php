<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 12.01.15
 * Time: 17:03
 */

namespace CMS\MenuBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MenuItemsController extends Controller
{
    public function listAction(){
        $service = $this->get('cms.menu.items.service');

        return $this->render('MenuBundle:MenuItems:list.html.twig', array(
            'items' => $service->findAll()
        ));
    }

    public function createAction(Request $request)
    {
        $service = $this->get('cms.menu.items.service');

        $form = $service->generateForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();

                $service->create($data);

                return $this->redirect($this->generateUrl('cms_menu_list'));
            }
        }

        return $this->render('MenuBundle:MenuItems:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function editAction(Request $request, $id)
    {
        $service = $this->get('cms.menu.items.service');

        $form = $service->generateForm($service->findOneById($id));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();

                $service->create($data);

                return $this->redirect($this->generateUrl('cms_menu_list'));
            }
        }

        return $this->render('MenuBundle:MenuItems:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function deleteAction($id)
    {
        $service = $this->get('cms.menu.items.service');

        $service->delete($id);

        return $this->redirect($this->generateUrl('cms_menu_list'));
    }
}