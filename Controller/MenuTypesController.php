<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 12.01.15
 * Time: 17:03
 */

namespace CMS\MenuBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MenuTypesController extends Controller
{
    public function listAction()
    {
        $service = $this->get('cms.menu.types.service');

        return $this->render('MenuBundle:MenuTypes:list.html.twig', array(
            'types' => $service->findAll()
        ));
    }

    public function createAction(Request $request)
    {
        $service = $this->get('cms.menu.types.service');

        $form = $service->generateForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();

                $service->create($data);

                return $this->redirect($this->generateUrl('cms_menu_types_list'));
            }
        }

        return $this->render('MenuBundle:MenuTypes:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function editAction(Request $request, $id)
    {
        $service = $this->get('cms.menu.types.service');

        $form = $service->generateForm($service->findOneById($id));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();

                $service->create($data);

                return $this->redirect($this->generateUrl('cms_menu_types_list'));
            }
        }

        return $this->render('MenuBundle:MenuTypes:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function deleteAction($id)
    {
        $service = $this->get('cms.menu.types.service');

        $service->delete($id);

        return $this->redirect($this->generateUrl('cms_menu_types_list'));
    }
}